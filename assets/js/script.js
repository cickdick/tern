/* Author: mohamad randi andika

*/
$(document).ready(function() {

		
	//Wrapper
	wrappers();

	function wrappers() {
		var windheight = $(window).height();
		var headheight = $('.app-header').outerHeight();
		var footheight = $('.app-footer').outerHeight();
		var wrap = $('.wrapper'); 
		wrap.css({
			'min-height': windheight,
			'padding-bottom': footheight - 20,
			'padding-top': headheight - 20,
			'position': 'relative',
		})
	}
	$(window).resize(function() {
		wrappers();
	});
	
	//Fullpage
	fullpage();

	function fullpage() {
		var windheight = $(window).height();
		var headheight = $('.app-header').outerHeight();
		var footheight = $('.app-footer').outerHeight();
		var wrap = $('.fullpage');
		wrap.css({
			'height': windheight,
			'padding-bottom': footheight - 0,
			'padding-top': headheight - 0,
			'position': 'relative',
		})
	}
	$(window).resize(function() {
		fullpage();
	});
	
	
	
	
});


// logo
function init() {
    window.addEventListener('scroll', function(e){
        var distanceY = window.pageYOffset || document.documentElement.scrollTop,
            shrinkOn = 300,
            header = document.querySelector("header");
        if (distanceY > shrinkOn) {
            classie.add(header,"smaller");
        } else {
            if (classie.has(header,"smaller")) {
                classie.remove(header,"smaller");
            }
        }
    });
}
window.onload = init();


// navscroll
jQuery(window).scroll(function() {
    var scroll = jQuery(window).scrollTop();
    if (scroll >= 313) {
        jQuery(".wrap-navi .nav li a").addClass("pt");
    } else {
        jQuery(".wrap-navi .nav li a").removeClass("pt");
    }
});

jQuery(window).scroll(function() {
    var scroll = jQuery(window).scrollTop();
    if (scroll >= 313) {
        jQuery(".wrap-navi .nav li.socmed").addClass("pts");
    } else {
        jQuery(".wrap-navi .nav li.socmed").removeClass("pts");
    }
});




// // equal hight
$(window).on("load", function() {
    var tallest = 0;
    if ($(window).width()) {
        $(".eqHeight").each(function() {
            var thisHeight = $(this).innerHeight();
            if (thisHeight > tallest) {
                tallest = thisHeight;
            }
        });
        $(".eqHeight").innerHeight(tallest);
    }
});



// wider shorter image
$(document).on({
      mouseenter: function () {
        //stuff to do on mouse enter
        $(".section2-1").addClass("wider");
        $(".section2-2").addClass("shorter");
      },
      mouseleave: function () {
        //stuff to do on mouse leave
        $(".section2-1").removeClass("wider");
        $(".section2-2").removeClass("shorter");
      }
    }, ".section2-1"); //pass the element as an argument to .on
    $(document).on({
      mouseenter: function () {
        //stuff to do on mouse enter
        $(".section2-2").addClass("wider");
        $(".section2-1").addClass("shorter");
      },
      mouseleave: function () {
        //stuff to do on mouse leave
        $(".section2-2").removeClass("wider");
        $(".section2-1").removeClass("shorter");
      }
    }, ".section2-2"); //pass the element as an argument to .on




    // owl carousel
 $(document).ready(function() {

  var sync1 = $("#sync1");
  var sync2 = $("#sync2");
  var slidesPerPage = 4; //globaly define number of elements per page
  var syncedSecondary = true;

  sync1.owlCarousel({
    items : 1,
    slideSpeed : 9000,
    nav: true,
    autoplay: false,
    dots: false,
    loop: true,
    responsiveRefreshRate : 200,
    
  }).on('changed.owl.carousel', syncPosition);

  sync2
    .on('initialized.owl.carousel', function () {
      sync2.find(".owl-item").eq(0).addClass("current");
    })
    .owlCarousel({
    // items : 4,
    dots:false,
    items : 1, // THIS IS IMPORTANT
      responsive : {
            480 : { items : 1  }, // from zero to 480 screen width 4 items
            768 : { items : 2  }, // from 480 screen widthto 768 6 items
            1024 : { items : 4   // from 768 screen width to 1024 8 items
            }
        },
  }).on('changed.owl.carousel', syncPosition2);

  function syncPosition(el) {
    //if you set loop to false, you have to restore this next line
    //var current = el.item.index;
    
    //if you disable loop you have to comment this block
    var count = el.item.count-1;
    var current = Math.round(el.item.index - (el.item.count/2) - .5);
    
    if(current < 0) {
      current = count;
    }
    if(current > count) {
      current = 0;
    }
    
    //end block

    sync2
      .find(".owl-item")
      .removeClass("current")
      .eq(current)
      .addClass("current");
    var onscreen = sync2.find('.owl-item.active').length - 1;
    var start = sync2.find('.owl-item.active').first().index();
    var end = sync2.find('.owl-item.active').last().index();
    
    if (current > end) {
      sync2.data('owl.carousel').to(current, 100, true);
    }
    if (current < start) {
      sync2.data('owl.carousel').to(current - onscreen, 100, true);
    }
  }
  
  function syncPosition2(el) {
    if(syncedSecondary) {
      var number = el.item.index;
      sync1.data('owl.carousel').to(number, 100, true);
    }
  }
  
  sync2.on("click", ".owl-item", function(e){
    e.preventDefault();
    var number = $(this).index();
    sync1.data('owl.carousel').to(number, 300, true);
  });
});

// slick
 $('.wrap-slide').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        dots: true,
        arrows: false,

        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true,
                dots: true
            }
        }, {
            breakpoint: 600,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }, {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: false,
                arrows: false
            }
        }]
    });

